from django.contrib import admin
from models import (
    Contact,
    Feature,
    Vendor,
    CPE,
    TestCase,
    Status,
    Not_Supported_Applicable,
    Mapping_CPE_TestCase,
    )
# Register your models here.


class CPE_Admin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('cpe_name',)}
    list_display = (
        'cpe_name',
        'vendor',
        'contact_person',
        )


class TestCase_Admin(admin.ModelAdmin):
    list_display = (
        't_id',
        'test_case_name',
        'feature',
        )
    search_fields = ['test_case_name']


class Mapping_Admin_pass_fail(admin.ModelAdmin):
    list_display = (
        'cpe',
        'test_case',
        'status',
        )
    search_fields = ['test_case']


class Mapping_Admin_na_ns(admin.ModelAdmin):
    list_display = (
        'cpe',
        'test_case',
        'status',
        )
    search_fields = ['test_case']


admin.site.register(Contact)
admin.site.register(Feature)
admin.site.register(Vendor)
admin.site.register(CPE, CPE_Admin)
admin.site.register(Status)
admin.site.register(TestCase, TestCase_Admin)
admin.site.register(Mapping_CPE_TestCase, Mapping_Admin_pass_fail)
admin.site.register(Not_Supported_Applicable, Mapping_Admin_na_ns)

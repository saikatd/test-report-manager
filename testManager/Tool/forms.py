from django import forms
from Tool.models import Feature


IMPORT_FILE_TYPES = ['.xls', ]

class Extract_Testcase_Form(forms.ModelForm):
    select_feature = forms.ModelChoiceField(
        queryset=Feature.objects.all(),
        empty_label="None")

    class Meta:
        model = Feature
        fields = ('select_feature',)


class FileUploadedForm(forms.Form):
    uploaded_file = forms.FileField(required=True, label="Upload the Report to the system.")


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField()
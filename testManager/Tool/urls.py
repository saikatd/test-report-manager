from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^all_cpe', views.all_cpe, name='all_cpe'),
    url(r'^test_repository', views.display_features,
        name='display_features'),
    url(r'^get_test_case/(?P<feature_id>[0-9]+)/$', views.get_test_case,
        name='get_test_case'),
    url(r'^feature_cpe/get_cpe/(?P<feature_ids>[\w,]+)/$', views.get_cpe,
        name='get_cpe'),
    url(r'^feature_cpe/$', views.filter_features,
        name='filter_features'),
    url(r'^feature_cpe/cpe_test_case/(?P<cpe_id>[0-9]+)/$',
        views.cpe_test_case, name='cpe_test_case'),

    url(r'^feature_cpe/cpe_test_case/(?P<cpe_id>[0-9]+)/(?P<status_string>[\w,]+)/$',
        views.cpe_test_case_status_based, name='cpe_test_case_cpe_based'),
    url(r'^search/cpe_test_case/(?P<cpe_id>[0-9]+)/(?P<status_string>[\w,]+)/$',
        views.cpe_test_case_status_based, name='cpe_test_case_cpe_based'),

    url(r'^search/cpe_test_case/(?P<cpe_id>[0-9]+)/$',
        views.cpe_test_case, name='cpe_test_case'),
    url(r'^search/$', views.show_search, name='show_search'),
    url(r'^search/get_cpe/(?P<search_string>[\w,]+)/$', views.get_search_cpe,
        name='get_search_cpe'),
    url(r'^upload/$', views.upload_excel_cpe_form, name='upload_excel_cpe_form'),
    url(r'^upload_testcase/$', views.upload_excel_testcase, name='upload_excel_testcase'),
    url(r'^home', views.trm_home, name='trm_home')
]


# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Tool', '0003_auto_20150821_1000'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testcase',
            name='feature',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, blank=True, to='Tool.Feature', null=True),
        ),
        migrations.AlterField(
            model_name='testcase',
            name='test_case_name',
            field=models.CharField(max_length=400, null=True, blank=True),
        ),
    ]

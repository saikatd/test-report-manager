# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Tool', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mapping_cpe_testcase',
            name='comment',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='mapping_cpe_testcase',
            name='cpe',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, blank=True, to='Tool.CPE', null=True),
        ),
        migrations.AlterField(
            model_name='mapping_cpe_testcase',
            name='status',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, blank=True, to='Tool.Status', null=True),
        ),
        migrations.AlterField(
            model_name='mapping_cpe_testcase',
            name='test_case',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, blank=True, to='Tool.TestCase', null=True),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('contact_name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='CPE',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cpe_name', models.CharField(max_length=100)),
                ('cpe_release', models.CharField(max_length=255, null=True, blank=True)),
                ('slug', models.SlugField(unique=True, null=True, blank=True)),
                ('cpe_description', models.TextField(max_length=500, null=True, blank=True)),
                ('cts_release', models.CharField(max_length=100, null=True, blank=True)),
                ('customer', models.CharField(max_length=200, null=True, blank=True)),
                ('contact_person', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, blank=True, to='Tool.Contact', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Feature',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('feature_name', models.CharField(max_length=300)),
            ],
        ),
        migrations.CreateModel(
            name='Mapping_CPE_TestCase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comment', models.CharField(max_length=200)),
                ('cpe', models.ForeignKey(to='Tool.CPE', on_delete=django.db.models.deletion.DO_NOTHING)),
            ],
        ),
        migrations.CreateModel(
            name='Not_Supported_Applicable',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comment', models.CharField(max_length=200, null=True, blank=True)),
                ('cpe', models.ForeignKey(to='Tool.CPE', on_delete=django.db.models.deletion.DO_NOTHING)),
            ],
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status_name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='TestCase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('t_id', models.IntegerField(unique=True)),
                ('test_case_name', models.CharField(max_length=400)),
                ('description', models.TextField(max_length=500, null=True, blank=True)),
                ('feature', models.ForeignKey(to='Tool.Feature', on_delete=django.db.models.deletion.DO_NOTHING)),
            ],
        ),
        migrations.CreateModel(
            name='Vendor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('vendor_name', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='not_supported_applicable',
            name='status',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, blank=True, to='Tool.Status', null=True),
        ),
        migrations.AddField(
            model_name='not_supported_applicable',
            name='test_case',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, blank=True, to='Tool.TestCase', null=True),
        ),
        migrations.AddField(
            model_name='mapping_cpe_testcase',
            name='status',
            field=models.ForeignKey(to='Tool.Status', on_delete=django.db.models.deletion.DO_NOTHING),
        ),
        migrations.AddField(
            model_name='mapping_cpe_testcase',
            name='test_case',
            field=models.ForeignKey(to='Tool.TestCase', on_delete=django.db.models.deletion.DO_NOTHING),
        ),
        migrations.AddField(
            model_name='cpe',
            name='vendor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, blank=True, to='Tool.Vendor', null=True),
        ),
    ]

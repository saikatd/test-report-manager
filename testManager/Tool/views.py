from django.shortcuts import render
from models import TestCase
from Tool.models import *
import simplejson
from django.http import HttpResponse
from django.db.models import Q
from forms import FileUploadedForm
from xlrd import open_workbook
from django.contrib.auth.decorators import login_required

########################################################### HOME PAGE ##################################################################

def trm_home(request):
    """displays the TRM Homepage"""

    return render(request, 'Tool/home.html')


########################################################### All CPE ##################################################################

def all_cpe(request):
    """displays all the available cpes"""

    cpes = CPE.objects.all()
    return render(request, 'Tool/cpes.html',
                  {'cpes': cpes})


######################################################### FEATURE BASED CPE #############################################################

def filter_features(request):
    """
    Displays the set of available features as checkboxes and based on selection
    a list of CPEs are populated
    """

    feature = Feature.objects.all().order_by('feature_name')
    return render(request, 'Tool/feature_cpe.html', {'feature': feature})



def get_cpe(request, feature_ids):
    """
    Return the JSON format of list of test cases
    corresponding to a given feature
    INPUT: [feature_id1, feature_id2,..]
    OUTPUT: [{'cpe_id':1, 'cpe_name':'cpe1'..},...]
    """
    cpes = feature_cpe(feature_ids.split(','))
    cpe_list = []
    for cpe in cpes:
        d = {}
        d['cpe_id'] = cpe.id
        d['cpe_name'] = cpe.cpe_name
        d['cpe_release'] = cpe.cpe_release
        d['description'] = cpe.cpe_description
        d['vendor'] = cpe.vendor.vendor_name
        d['cts_release'] = cpe.cts_release
        d['contact_person'] = cpe.contact_person.contact_name
        d['customer'] = cpe.customer
        cpe_list.append(d)
    return HttpResponse(simplejson.dumps(cpe_list),
                        content_type="application/json")


def feature_cpe(feature):
    """
    returns all the cpes with given feature code
    feature = ['f_id1', 'f_id2',....] a list of feature_names
    """

    # forming the query ->
    # gets all the cpe_ids that match with the given set of features
    cpe_id_feature = Mapping_CPE_TestCase.objects.filter(
        test_case__feature__id__in=feature).values(
        'cpe', 'test_case__feature').distinct()

    # {<cpe_id>:set(<f1_id>,<f2_id>)}
    d = {}
    for l in cpe_id_feature:
        temp_set = d.get(l['cpe'], set())
        temp_set.add(l['test_case__feature'])
        d[l['cpe']] = temp_set

    # finding the cpe_ids which have same number of features
    no_of_features = len(set(feature))
    cpe_ids = []
    for key in d:
        if no_of_features == len(d[key]):
            cpe_ids.append(key)
    cpes = CPE.objects.filter(id__in=cpe_ids)
    return cpes


################################################### CPE TEST CASE PAGE ################################################################

def cpe_test_case(request, cpe_id):
    """
    returns the list of test cases and the status of each of them
    for a given cpe
    cpe_id = cpe_id (Integer)
    """

    # test cases related to PASS/FAIL/BLOCKED
    test_case_objs_pass_failed = Mapping_CPE_TestCase.objects.filter(
        cpe__id=cpe_id)

    # test cases related to NA/NS
    test_case_objs_na_ns = Not_Supported_Applicable.objects.filter(
        cpe__id=cpe_id)

    test_case_list = {}
    for test in test_case_objs_pass_failed:
        test_case = {}
        test_case['test_id'] = test.test_case.t_id
        test_case['test_case_name'] = test.test_case.test_case_name
        test_case['status'] = test.status
        test_case['comment'] = test.comment
        feature_test_case_list = test_case_list.get(test.test_case.feature.feature_name, [])
        feature_test_case_list.append(test_case)
        test_case_list[test.test_case.feature.feature_name] = feature_test_case_list

    for test in test_case_objs_na_ns:
        test_case = {}
        test_case['test_id'] = test.test_case.t_id
        test_case['test_case_name'] = test.test_case.test_case_name
        test_case['status'] = test.status
        test_case['comment'] = test.comment
        feature_test_case_list = test_case_list.get(test.test_case.feature.feature_name, [])
        feature_test_case_list.append(test_case)
        test_case_list[test.test_case.feature.feature_name] = feature_test_case_list

    cpe_details = CPE.objects.get(id=cpe_id)
    status = Status.objects.all()

    return render(request, 'Tool/test_cases.html',
                  {'test_case_list': test_case_list,
                  'cpe_details': cpe_details,
                  'status': status})


def cpe_test_case_status_based(request, cpe_id, status_string):
    """
    returns the list of test cases based on the status_string given a cpe
    INPUT: cpe_id = cpe_id (Integer)
           status_string = "0,1,2" 0, 1 and 2 are status ids whose results are desired
    OUTPUT: [{'test_case_name': <test_case_name>, 'status': <status_name>,..},...]
    """


    # status string is split to get the status ids in list form
    status_list = status_string.split(",")

    # test cases related to PASS/FAIL/BLOCKED
    test_case_objs_pass_failed = Mapping_CPE_TestCase.objects.filter(
        cpe__id=cpe_id, status__id__in=(status_list)).order_by('test_case__feature__feature_name')

    # test cases related to NA/NS
    test_case_objs_na_ns = Not_Supported_Applicable.objects.filter(
        cpe__id=cpe_id, status__id__in=(status_list)).order_by('test_case__feature__feature_name')

    test_case_list = {}
    for test in test_case_objs_pass_failed:
        test_case = {}
        test_case['test_id'] = test.test_case.t_id
        test_case['test_case_name'] = test.test_case.test_case_name
        test_case['status'] = test.status.status_name
        test_case['comment'] = test.comment
        feature_test_case_list = test_case_list.get(test.test_case.feature.feature_name, [])
        feature_test_case_list.append(test_case)
        test_case_list[test.test_case.feature.feature_name] = feature_test_case_list

    for test in test_case_objs_na_ns:
        test_case = {}
        test_case['test_id'] = test.test_case.t_id
        test_case['test_case_name'] = test.test_case.test_case_name
        test_case['status'] = test.status.status_name
        test_case['comment'] = test.comment
        feature_test_case_list = test_case_list.get(test.test_case.feature.feature_name, [])
        feature_test_case_list.append(test_case)
        test_case_list[test.test_case.feature.feature_name] = feature_test_case_list

    return HttpResponse(simplejson.dumps(test_case_list),
                        content_type="application/json")


###################################################### TEST REPOSITORY ################################################################

def display_features(request):
    """
    Obtains the list of features and shows them as part of dropdown
    OUTPUT: {'feature':<feature_object>}
    """

    feature = Feature.objects.all().order_by('feature_name')
    return render(request, 'Tool/feature_test_cases.html',
                  {'feature': feature})


def get_test_case(request, feature_id):
    """
    Return the JSON format of list of test cases
    corresponding to a given feature
    INPUT: feature_id = <f_id> of model Feature
    OUTPUT: [{'test_id':1, 'test_case_name':'t1'..},...]
    """

    test_case_object = TestCase.objects.filter(
        feature__id=feature_id).order_by('id')
    test_case_list = []
    for test in test_case_object:
        test_case = {}
        test_case['test_id'] = test.id
        test_case['test_case_name'] = test.test_case_name
        test_case['test_case_description'] = test.description
        test_case_list.append(test_case)
    feature_details = {}
    feature_details['feature_name'] = Feature.objects.get(id=feature_id).feature_name
    test_case_list.insert(0,feature_details)
    return HttpResponse(simplejson.dumps(test_case_list),
                        content_type="application/json")


###################################################### SEARCH CPE ################################################################

def show_search(request):
    """
    Displays the search page for searching by CPE name, Vendor,
    or contact person
    """

    return render(request, 'Tool/search_cpe.html', {})


def get_search_cpe(request, search_string):
    """
    INPUT: search_string : 'search_string'
    Search CPEs by cpe_name, vendor, cts release
    """

    cpes = CPE.objects.filter(Q(cpe_name__icontains=search_string) |
                              Q(vendor__vendor_name__icontains=search_string) |
                              Q(contact_person__contact_name__icontains=search_string))

    cpe_list = []
    for cpe in cpes:
        d = {}
        d['cpe_id'] = cpe.id
        d['cpe_name'] = cpe.cpe_name
        d['cpe_release'] = cpe.cpe_release
        d['description'] = cpe.cpe_description
        d['customer'] = cpe.customer
        d['cpe_release'] = cpe.cpe_release
        d['vendor'] = cpe.vendor.vendor_name
        d['cts_release'] = cpe.cts_release
        d['contact_person'] = cpe.contact_person.contact_name

        cpe_list.append(d)
    return HttpResponse(simplejson.dumps(cpe_list),
                        content_type="application/json")


###################################################### SEARCH CPE ################################################################

@login_required
def upload_excel_testcase(request):
    """
    Excel parser for Test Case repository
    """

    if request.method == 'POST':
        form = FileUploadedForm(request.POST, request.FILES)
        if form.is_valid():
            input_file = request.FILES.get('uploaded_file')
            wb = open_workbook(file_contents=input_file.read())
            # constant variables as per Excel sheet
            col_tid = 0
            col_tname = 1
            col_feature = 2

            # variables to store rejected sheets and test cases
            rejected_testcases = set()

            # only first sheet contains the test case and feature mapping details
            s = wb.sheet_by_index(0)

            for row in range(s.nrows):
                if type(s.cell(row,col_tid).value) == float and s.ncols == 3:
                    if Feature.objects.filter(feature_name=s.cell(row, col_feature).value).exists():
                        # Feature exists in the database
                        feature = Feature.objects.filter(feature_name=s.cell(row, col_feature).value)[0]
                        test_case_obj, created = TestCase.objects.get_or_create(t_id=s.cell(row, col_tid).value)
                        test_case_obj.test_case_name = s.cell(row,col_tname).value
                        test_case_obj.feature = feature
                        test_case_obj.save()
                    else:
                        # Feature does not exist in database
                        rejected_testcases.add((int(s.cell(row, col_tid).value),
                                                s.cell(row, col_tname).value,
                                                s.cell(row, col_feature).value))
                else:
                    return render(request, 'Tool/invalid_excel.html')

            return render(request, 'Tool/post_upload_testcase.html',
                          {'rejected_testcases': rejected_testcases})   
        else:
            form = FileUploadedForm()
            return render(request, 'Tool/upload_excel.html', {'form': form})
    else:
        form = FileUploadedForm()
        return render(request, 'Tool/upload_excel.html', {'form': form})



def upload_excel_cpe_form(request):
    """
    Excel parser for CPE test report
    """

    if request.method == 'POST':
        form = FileUploadedForm(request.POST, request.FILES)
        if form.is_valid():
            input_file = request.FILES.get('uploaded_file')
            wb = open_workbook(file_contents=input_file.read())
            # constant variables as per Excel sheet
            col_status = 2
            col_tid = 0
            col_comment = 3
            col_tname = 1

            status_dict = {}
            status_list = Status.objects.all()
            for obj in status_list:
               status_dict[obj.status_name]=obj
            # variables to store rejected sheets and test cases
            rejected_cpe = []
            rejected_testcases = set()
            # s = wb.sheet_by_index(1)
            for s in wb.sheets():

                # Checking if the CPE name exists
                if CPE.objects.filter(cpe_name=s.name).exists() and s.ncols == 4:
                    cpe = CPE.objects.get(cpe_name=s.name)
                    for row in range(s.nrows):
                        if type(s.cell(row,col_tid).value) == float:
                            if TestCase.objects.filter(t_id=s.cell(row,col_tid).value).exists():
                                # test case exists in the database
                                test_case = TestCase.objects.get(t_id=s.cell(row, col_tid).value)
                                if s.cell(row, col_status).value.lower() == 'na' or s.cell(row, col_status).value.lower() == 'ns':
                                    # na and ns test cases go to Not_Supported_Applicable table forming a cpe<->test case mapping
                                    obj, created = Not_Supported_Applicable.objects.get_or_create(cpe=cpe, test_case=test_case)
                                else:
                                    # all other test cases - pass/fail/blocked/passx go to Mapping_CPE_TestCase table forming a cpe<->test case mapping
                                    obj, created = Mapping_CPE_TestCase.objects.get_or_create(cpe=cpe, test_case=test_case)
                                if s.cell(row, col_status).value != "" and status_dict.get(s.cell(row, col_status).value,0) != 0:
                                    obj.status = status_dict[s.cell(row, col_status).value.lower()]
                                    obj.comment = s.cell(row, col_comment).value
                                    obj.save()
                                else:
                                    # status doesnot exist in database
                                    rejected_testcases.add((s.name,
                                                        int(s.cell(row, col_tid).value),
                                                        s.cell(row, col_tname).value,
                                                        s.cell(row, col_status).value,
                                                        s.cell(row, col_comment).value))
                                    
                            else:
                                # test case does not exist in database
                                rejected_testcases.add((s.name,
                                                        int(s.cell(row, col_tid).value),
                                                        s.cell(row, col_tname).value,
                                                        s.cell(row, col_status).value,
                                                        s.cell(row, col_comment).value))
                else:
                    # error in cpe_name in the sheet name
                    rejected_cpe.append(s.name)
            return render(request, 'Tool/post_upload.html',
                          {'rejected_cpe': rejected_cpe,
                           'rejected_testcases': rejected_testcases})
        else:
            form = FileUploadedForm()
            return render(request, 'Tool/upload_excel.html', {'form': form})
    else:
        form = FileUploadedForm()
        return render(request, 'Tool/upload_excel.html', {'form': form})

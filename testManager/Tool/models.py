from django.db import models

# Create your models here.


class Contact(models.Model):
    """List of contact point for a given cpe"""

    contact_name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.contact_name


class Feature(models.Model):
    """Model for Feature"""

    feature_name = models.CharField(max_length=255, unique=True)

    def __unicode__(self):
        return self.feature_name


class Vendor(models.Model):
    """List of vendors"""

    vendor_name = models.CharField(max_length=100, unique=True)

    def __unicode__(self):
        return self.vendor_name


class TestCase(models.Model):
    """List of test cases"""

    t_id = models.IntegerField(unique=True)
    test_case_name = models.CharField(max_length=400, null=True, blank=True)
    description = models.TextField(max_length=500, null=True, blank=True)
    feature = models.ForeignKey(Feature, on_delete=models.DO_NOTHING, null=True, blank=True)

    def __unicode__(self):
        return "Test Case ID:{0} | Feature:{1}".format(
            self.t_id,
            self.feature.feature_name)


class CPE(models.Model):
    """List of CPEs"""

    cpe_name = models.CharField(max_length=100, unique=True)
    cpe_release = models.CharField(max_length=255, null=True, blank=True)
    slug = models.SlugField(unique=True, null=True, blank=True)
    cpe_description = models.TextField(max_length=500, null=True, blank=True)
    vendor = models.ForeignKey(Vendor,
                               on_delete=models.DO_NOTHING,
                               null=True, blank=True)
    cts_release = models.CharField(max_length=100, null=True, blank=True)
    customer = models.CharField(max_length=200, null=True, blank=True)
    contact_person = models.ForeignKey(
            Contact, on_delete=models.DO_NOTHING, null=True, blank=True)

    def __unicode__(self):
        return "CPE:{0} | Release:{1}".format(
            self.cpe_name, self.cpe_release)


class Status(models.Model):
    """List of Status"""

    status_name = models.CharField(max_length=200, unique=True)

    def __unicode__(self):
        return self.status_name


class Mapping_CPE_TestCase(models.Model):
    """Mapping of CPE and TestCase"""

    cpe = models.ForeignKey(CPE, on_delete=models.DO_NOTHING, blank=True, null=True)
    test_case = models.ForeignKey(TestCase, on_delete=models.DO_NOTHING, blank=True, null=True)
    status = models.ForeignKey(Status, on_delete=models.DO_NOTHING, blank=True, null=True)
    comment = models.CharField(max_length=200, blank=True, null=True)

    def __unicode__(self):
        return "CPE: {0}".format(
            self.cpe.cpe_name)


class Not_Supported_Applicable(models.Model):
    """Mapping for CPE and test cases where test case status is
    NA or NS"""

    cpe = models.ForeignKey(CPE, on_delete=models.DO_NOTHING)
    test_case = models.ForeignKey(TestCase, on_delete=models.DO_NOTHING,
                                  blank=True, null=True)
    status = models.ForeignKey(Status, on_delete=models.DO_NOTHING,
                               blank=True, null=True)
    comment = models.CharField(max_length=200, blank=True, null=True)

    def __unicode__(self):
        return "CPE: {0}".format(
            self.cpe.cpe_name)
